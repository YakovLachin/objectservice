CWD=/go/src/gitlab.com/YakovLachin/objectservice
IMAGE=gitlab.com/yakovlachin/objectservice
CODE_GENERATOR=grpc-go
test:
	@docker run --rm -v $(CURDIR):/app -w /app golang:1.8-alpine go test ./...

format:
	@docker run --rm -v $(CURDIR):/app -w /app golang:1.8-alpine gofmt -w /app

image: service
	@docker build -t $(IMAGE) .

up: create-network
	@docker-compose up -d

dev_up: create-network
	@docker-compose -f dev-docker-compose.yml up -d

service:
	@docker run --rm -it -v $(CURDIR):$(CURDIR) -w $(CURDIR) \
	grpc-go /bin/bash -c 'mkdir service && protoc --proto_path=./proto/ --go_out=plugins=grpc:./service service.proto'

unit_test:
	@docker run --rm -v $(CURDIR):$(CWD) -w $(CWD) golang:1.8-alpine \
		sh -c "go list ./... | grep -v 'vendor\|acceptance' | xargs go test"

smoke_test:
	@docker run --rm \
	--network cube-network \
	-e MYSQL=true \
	-e MYSQL_HOST=objectsql \
    -e MYSQL_PORT=3306 \
    -e MYSQL_USER=root \
    -e MYSQL_DBNAME=objects \
    -e MYSQL_PASS=qwerty \
    gitlab.com/yakovlachin/smoke-test:latest

create-network:
	@-docker network create cube-network

acceptance: clean up smoke_test
	@docker run --rm \
	--network cube-network \
	-v $(CURDIR):$(CWD) -w $(CWD) \
	-e TEST_HOST=objectservice \
	-e TEST_PORT=8080 \
	golang:1.8-alpine \
	sh -c "go test -v ./acceptance"

clean:
	@-sh -c 'docker ps -aq | xargs docker rm -fv'

.PHONY: acceptance

.DEFAULT_GOAL := image