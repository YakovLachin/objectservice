FROM golang:1.8.3-alpine as builder
WORKDIR /go/src/gitlab.com/YakovLachin/objectservice
COPY . .
RUN GOOS=linux GOARCH=amd64 go build -v -o rel/service

FROM alpine:3.6
WORKDIR /service
EXPOSE 8080
EXPOSE 9000
COPY --from=builder /go/src/gitlab.com/YakovLachin/objectservice/rel/service ./
CMD ["./service"]
