package server

import (
	"gitlab.com/YakovLachin/objectservice/service"
	"testing"
	"github.com/stretchr/testify/assert"
	"gitlab.com/YakovLachin/objectservice/uuid"
)

func Test_getObjIdsFromReq(t *testing.T)  {
	expected := []uuid.UID{
		uuid.UID("uid1"),
		uuid.UID("uid2"),
	}
	req := &service.ListObjectRequest{
		[]*service.Object{
				{
					Uid: "uid1",
				},
				{
					Uid: "uid2",
				},
			},
		}
	actual := getObjUIDsFromReq(req)
	assert.Equal(t, expected, actual)
}