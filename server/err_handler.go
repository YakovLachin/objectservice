package server

import "fmt"

func errHandle(e error, msg string) {
	if e != nil {
		e = fmt.Errorf("%s: %s", msg, e.Error())
	}
}
