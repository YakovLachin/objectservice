package server

import (
	"net/http"
	"gitlab.com/YakovLachin/objectservice/service"
	"github.com/golang/protobuf/jsonpb"
	"io/ioutil"
	"context"
	"google.golang.org/grpc/status"
	"google.golang.org/grpc/codes"
)

type HttpServerAdapter struct {
	GrpcSrv service.ObjectServiceServer
}

func newHttpAdapter(grpcSrv service.ObjectServiceServer) *HttpServerAdapter{
	return &HttpServerAdapter{
		GrpcSrv: grpcSrv,
	}
}

func (srv *HttpServerAdapter) ListObject(writer http.ResponseWriter, req *http.Request) {
	body, err := ioutil.ReadAll(req.Body)
	if err != nil {
		writer.WriteHeader(http.StatusBadRequest)
		writer.Write([]byte("FAIL GET BODY:" + err.Error()))

		return
	}

	grpcReq	:= service.ListObjectRequest{}
	err = jsonpb.UnmarshalString(string(body), &grpcReq)

	if err != nil {
		writer.WriteHeader(http.StatusBadRequest)
		writer.Write([]byte("FAIL UNMARSHALING:" + err.Error()))
		return
	}

	ctx := context.Background()
	resp, err:= srv.GrpcSrv.ListObject(ctx, &grpcReq)
	if c, ok := status.FromError(err); !ok || c.Code() == codes.Internal {
		writer.WriteHeader(http.StatusInternalServerError)
		writer.Write([]byte("FAIL LIST OBJECT:" + c.Message()))
		return
	} else if c.Code() == codes.AlreadyExists {
		writer.WriteHeader(http.StatusConflict)
		writer.Write([]byte("FAIL LIST OBJECT:" + c.Message()))
		return
	} else if c.Code() == codes.InvalidArgument {
		writer.WriteHeader(http.StatusBadRequest)
		writer.Write([]byte("FAIL GET BODY:" + err.Error()))
		return
	}

	marshaler := jsonpb.Marshaler{}
	res, err := marshaler.MarshalToString(resp)
	writer.Write([]byte(res))
}

func (srv *HttpServerAdapter) CreateObject(writer http.ResponseWriter, req *http.Request) {
	body, err := ioutil.ReadAll(req.Body)
	if err != nil {
		writer.WriteHeader(http.StatusBadRequest)
		writer.Write([]byte("FAIL GET BODY:" + err.Error()))

		return
	}

	grpcReq	:= service.CreateObjectRequest{}
	err = jsonpb.UnmarshalString(string(body), &grpcReq)

	if err != nil {
		writer.WriteHeader(http.StatusBadRequest)
		writer.Write([]byte("FAIL UNMARSHALING HR :" + err.Error()))
		return
	}

	ctx := context.Background()
	resp, err:= srv.GrpcSrv.CreateObject(ctx, &grpcReq)
	if c, ok := status.FromError(err); !ok || c.Code() == codes.Internal {
		writer.WriteHeader(http.StatusInternalServerError)
		writer.Write([]byte("FAIL CREATE OBJECT:" + c.Message()))
		return
	} else if c.Code() == codes.AlreadyExists {
		writer.WriteHeader(http.StatusConflict)
		writer.Write([]byte("FAIL CREATE OBJECT:" + c.Message()))
		return
	} else if c.Code() == codes.InvalidArgument {
		writer.WriteHeader(http.StatusBadRequest)
		writer.Write([]byte("FAIL GET BODY:" + err.Error()))
		return
	}

	marshaler := jsonpb.Marshaler{}
	res, err := marshaler.MarshalToString(resp)
	writer.WriteHeader(http.StatusCreated)
	writer.Write([]byte(res))
}
