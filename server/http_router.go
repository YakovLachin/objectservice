package server

import (
	"net/http"
	"gitlab.com/YakovLachin/objectservice/service"
)

func NewMux(grpcSrv service.ObjectServiceServer) *http.ServeMux {
    adapter := newHttpAdapter(grpcSrv)
	mux := http.NewServeMux()
	mux.HandleFunc("/object/list", adapter.ListObject)
	mux.HandleFunc("/object/create", adapter.CreateObject)

	return mux
}
