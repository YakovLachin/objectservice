package server

import (
	"gitlab.com/YakovLachin/objectservice/service"
	"gitlab.com/YakovLachin/objectservice/repository"
	"gitlab.com/YakovLachin/objectservice/auth"
	"golang.org/x/net/context"
	"google.golang.org/grpc/status"
	"google.golang.org/grpc/codes"
)

type srv struct {
	repo repository.Repo
	tokenFactory auth.TokenFactory
}

func (s *srv) CreateObject(ctx context.Context, req *service.CreateObjectRequest) (res *service.CreateObjectResponse, err error) {
	defer errHandle(err, "Create Object")
	rec, err := repository.RecordFromObject(req.Object)
	if err != nil {
		err = status.Error(codes.InvalidArgument, err.Error())
		return
	}

	err = s.repo.CreateObjectRec(rec)
	if err != nil {
		err = status.Error(codes.Internal, err.Error())
		return
	}

	return &service.CreateObjectResponse{Object:repository.ObectFromRecord(rec)}, nil
}

func (s *srv) ListObject(ctx context.Context,req *service.ListObjectRequest) (res *service.ListObjectResponse, err error) {
	uids := getObjUIDsFromReq(req)
	if len(uids) == 0 {
		return &service.ListObjectResponse{
			Objects: nil,
		}, nil
	}
	objs, err := s.repo.ListObjectRec(uids)
	if err != nil {
		err = status.Error(codes.Internal, err.Error())
		return
	}

	res = &service.ListObjectResponse{
		Objects:objs,
	}
	return
}

func NewServer(rep repository.Repo, tokenFactory auth.TokenFactory) service.ObjectServiceServer {
	return &srv{
		repo:rep,
		tokenFactory:tokenFactory,
	}
}
