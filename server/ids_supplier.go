package server

import (
	"gitlab.com/YakovLachin/objectservice/service"
	"gitlab.com/YakovLachin/objectservice/uuid"
)

func getObjUIDsFromReq(req *service.ListObjectRequest) []uuid.UID {
	ids := []uuid.UID{}
	for _, obj := range req.Objects {
		ids = append(ids, uuid.UID(obj.GetUid()))
	}

	return ids
}