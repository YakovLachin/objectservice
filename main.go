package main

import (
    "database/sql"
	"fmt"
	"net"
	"net/http"
	"os"

	_ "github.com/go-sql-driver/mysql"
	"github.com/sirupsen/logrus"
	"google.golang.org/grpc"

	"gitlab.com/YakovLachin/objectservice/config"
	"gitlab.com/YakovLachin/objectservice/repository"
	"gitlab.com/YakovLachin/objectservice/service"
	"gitlab.com/YakovLachin/objectservice/server"
	"time"
	"gitlab.com/YakovLachin/objectservice/auth"
)

func main() {
	log := logrus.New()
	log.Level = logrus.DebugLevel
	logger := log.WithField("channel", "object-server")
	logger.Info("Starting server")

	var opts []grpc.ServerOption
	grpcServ := grpc.NewServer(opts...)
	c, err := config.NewMysqlConfigFromEnv("OBJECT")
	db, err := sql.Open("mysql", c.String())
	if err != nil {
		logger.Fatalf("Fail connect to myslq %s", err)
	}
	objectRepo := repository.NewRepo(db)
	secret, err := config.GetJWTSecretKey("OBJECT")
	if err != nil {
		logger.Fatalf("Fail get JWT Secret %s", err)
	}
		tokenFactory := auth.NewTokenFactory(secret)
	srv := server.NewServer(objectRepo, tokenFactory)

	service.RegisterObjectServiceServer(grpcServ, srv)

	ntSrv, err := net.Listen("tcp", "0.0.0.0:9000")
	if err != nil {
		fmt.Println("Fail tcp Listen port")
		os.Exit(1)
	}

	go grpcServ.Serve(ntSrv)

	httpSrv := &http.Server{
		Addr:         "0.0.0.0:8080",
		Handler:      server.NewMux(srv),
		ReadTimeout:  time.Duration(5 * time.Second),
		WriteTimeout: time.Duration(5 * time.Second),
	}

	sigchan := make(chan os.Signal)
	go httpSrv.ListenAndServe()
	<-sigchan
}
