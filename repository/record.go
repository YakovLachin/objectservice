package repository

import "gitlab.com/YakovLachin/objectservice/uuid"

type ObjectRecord struct {
	UID uuid.UID
	Type int32
	Access int32
}