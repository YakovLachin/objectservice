package repository

import (
	"gitlab.com/YakovLachin/objectservice/uuid"
	"gitlab.com/YakovLachin/objectservice/service"
	"fmt"
)

func (r *Repo) ListObjectRec(uids []uuid.UID) (res []*service.Object, err error) {
	query := fmt.Sprintf("SELECT uid, type, access FROM objects WHERE uid IN (%s)", uidsForQuery(uids))
	rows, err := r.DB.Query(query)
	if err != nil {
		err = fmt.Errorf("fail query a list objects %s, query: %s", err.Error(), query)
		return
	}
	defer rows.Close()

	for rows.Next() {
		var rec ObjectRecord
		err := rows.Scan(&rec.UID, &rec.Type, &rec.Access)
		if err != nil {
			err = fmt.Errorf("Error Scan a row after get a list Objects: %s", err.Error())
		}

		res = append(res, ObectFromRecord(rec))
	}

	return
}
