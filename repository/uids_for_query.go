package repository

import (
	"gitlab.com/YakovLachin/objectservice/uuid"
)

func uidsForQuery(uids []uuid.UID) string {
	var res string
	for _, v := range uids {
		res = res + " UNHEX('" + v.ToString() + "'),"
	}
	if len(res) > 0 {
		res = res[1:len(res)-1]
	}
	return res
}