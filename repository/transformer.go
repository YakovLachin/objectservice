package repository

import (
	"gitlab.com/YakovLachin/objectservice/service"
	"gitlab.com/YakovLachin/objectservice/uuid"
	"reflect"
	"errors"
)

func RecordFromObject(object *service.Object) (rec ObjectRecord, err error) {
	if isNil(object) {
		err = errors.New("Fail Transforming. Object Is nil")
		return
	}

	rec = ObjectRecord{}
	if (object.Uid == "") {
		rec.UID = uuid.NewUID()
	} else {
		rec.UID = uuid.UID(object.Uid)
	}

	rec.Type = int32(object.Type)

	rec.Access = int32(object.Access)
	return
}

func ObectFromRecord(rec ObjectRecord) *service.Object {
	object := service.Object{}
	object.Access = service.Access(rec.Access)
	object.Type   = service.TypeOf(rec.Type)
	object.Uid    = rec.UID.ToString()

	return &object
}

func isNil(object interface{}) bool {
	if object == nil {
		return true
	}

	value := reflect.ValueOf(object)
	kind := value.Kind()
	if kind >= reflect.Chan && kind <= reflect.Slice && value.IsNil() {
		return true
	}

	return false
}
