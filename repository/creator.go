package repository

func (r *Repo) CreateObjectRec(record ObjectRecord) (err error) {
	_, err = r.DB.Exec("INSERT INTO objects (uid, type, access) VALUES(UNHEX(?), ?, ?)", record.UID, record.Type, record.Access)
	return
}
