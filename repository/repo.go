package repository

import "database/sql"

type Repo struct {
	DB *sql.DB
}

func NewRepo(db *sql.DB) Repo {
	return Repo{
		DB: db,
	}
}

