package repository

import (
	"testing"
	"github.com/stretchr/testify/assert"
	"gitlab.com/YakovLachin/objectservice/service"
	"gitlab.com/YakovLachin/objectservice/uuid"
)

func TestRecordFromObject_Positive(t *testing.T) {
	object := &service.Object{
		Type: service.TypeOf_FAKE,
		Access: service.Access_INTERNAL,
	}

	actual, err := RecordFromObject(object)
	assert.NoError(t, err)
	assert.Equal(t, int32(object.Type), actual.Type)
	assert.Equal(t, int32(object.Access), actual.Access)
	assert.NotEmpty(t, actual.UID)
}

func TestObjectFromRecord(t *testing.T) {
	uid := uuid.NewUID()
	rec := ObjectRecord{
		UID: uid,
		Type: int32(service.TypeOf_FAKE),
		Access: int32(service.Access_INTERNAL),
	}

	expected := &service.Object{
		Uid: uid.ToString(),
		Type: service.TypeOf_FAKE,
		Access: service.Access_INTERNAL,
	}
	actual := ObectFromRecord(rec)
	assert.Equal(t, expected, actual)
}