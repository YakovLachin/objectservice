package repository

import (
	"testing"
	"gitlab.com/YakovLachin/objectservice/uuid"
	"github.com/stretchr/testify/assert"
)

func TestUidsForQuery(t *testing.T) {
	uids := []uuid.UID{
		uuid.UID("1"),
		uuid.UID("0"),
	}

	expected := "UNHEX('1'), UNHEX('0')"
	actual := uidsForQuery(uids)
	assert.Equal(t, expected, actual)
}
