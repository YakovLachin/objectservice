package acceptance

import (
	"testing"
	"gitlab.com/YakovLachin/objectservice/service"
	"github.com/stretchr/testify/assert"
)

func TestStringToMessage_Positive(t *testing.T) {
	msg := `{"object":{"type":"CONTAINER"}}`
	expected := &service.CreateObjectRequest{
		Object: &service.Object{
			Type: service.TypeOf_CONTAINER,
		},
	}

	init := &service.CreateObjectRequest{}
	res, err := stringToMessage(msg, init)
	assert.NoError(t, err)
	assert.Equal(t, expected, res)
}
