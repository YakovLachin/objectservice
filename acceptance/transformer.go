package acceptance

import (
	"github.com/golang/protobuf/proto"
	"github.com/golang/protobuf/jsonpb"
	"errors"
	"reflect"
	"database/sql"
)

func messageToJSONString(pb proto.Message) (string, error) {
	marshaler := jsonpb.Marshaler{}
	if isNil(pb) {
		return "{}", errors.New("message struct is <nil>")
	}
	res, err := marshaler.MarshalToString(pb)

	if err != nil {
		return "{}", err
	}

	return res, nil
}

func isNil(object interface{}) bool {
	if object == nil {
		return true
	}

	value := reflect.ValueOf(object)
	kind := value.Kind()
	if kind >= reflect.Chan && kind <= reflect.Slice && value.IsNil() {
		return true
	}

	return false
}

func stringToMessage(str string, res proto.Message) (proto.Message, error) {

	err := jsonpb.UnmarshalString(str, res)

	return res, err
}

func nullStringToMessage(str sql.NullString, res proto.Message) (proto.Message, error) {
	if !str.Valid {
		return res, nil
	}

	return stringToMessage(str.String, res)
}
