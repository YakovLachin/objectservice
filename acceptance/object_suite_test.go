package acceptance

import (
	. "github.com/onsi/ginkgo"
	. "github.com/onsi/gomega"
	"bytes"
	"gitlab.com/YakovLachin/objectservice/service"
	"github.com/golang/protobuf/proto"
	"io"
	"io/ioutil"
	"net/http"
	"os"
	"testing"
)

var hostName string
func TestAcceptanceUserService(t *testing.T) {
	RegisterFailHandler(Fail)
	RunSpecs(t, "Работа с объектом")
}
var uid string
var _ = Describe("Работа с объектом.", func() {
	Describe("Создание", func() {
		object := &service.CreateObjectRequest{
			&service.Object{
					Type: service.TypeOf_CONTAINER,
					Access: service.Access_PUBLIC,
			},
		}
		body, _ := messageToJSONString(object)

		Context("Запрос с корректными данными: " + body, func() {
			req, err := http.NewRequest("POST",  getUrl("/object/create"), bytes.NewBuffer([]byte(body)))
			client := &http.Client{}
			resp, err := client.Do(req)

			It("Должен вернуться Код 201", func() {
				Expect(err).Should(BeNil())
				Expect(resp.StatusCode).Should(Equal(http.StatusCreated))
			})

			It("Должен вернуть ответ с идендифицированным объектом", func() {
				res := &service.CreateObjectResponse{}
				getMessageFromResponseBody(resp.Body, res)
				Expect(res.GetObject().GetUid()).ShouldNot(BeEmpty())
				uid = res.GetObject().GetUid()
			})
		})
	})
	Describe("Получение списка  объектов.", func () {
		obj := &service.ListObjectRequest{
				[]*service.Object{
					{
						Uid:    "85731ebe56924ee592f268df12b847ca",
						Type:   service.TypeOf_CONTAINER,
						Access: service.Access_PUBLIC,
					},
					{
						Uid:    "c21d91ea8c27493fa02c7a4c709e013d",
						Type:   service.TypeOf_CONTAINER,
						Access: service.Access_PUBLIC,
					},
					{
						Uid:    "be850d18695e433e884c2628a2b4aff2",
						Type:   service.TypeOf_CONTAINER,
						Access: service.Access_PUBLIC,
					},
				},
			}
		body, _ := messageToJSONString(obj)
		Context("Запрос с корректными данными: " + body, func() {
			req, err := http.NewRequest("POST",  getUrl("/object/list"), bytes.NewBuffer([]byte(body)))
			client := &http.Client{}
			resp, err := client.Do(req)
			It("Должен вернуться Код 200", func() {
				Expect(err).Should(BeNil())
				Expect(resp.StatusCode).Should(Equal(http.StatusOK))
			})
			It("Должен вернуть список объектов", func() {
				res := &service.ListObjectResponse{}
				getMessageFromResponseBody(resp.Body, res)
				expected := []*service.Object{
					{
						Uid:    "85731ebe56924ee592f268df12b847ca",
						Type:   service.TypeOf_CONTAINER,
						Access: service.Access_PUBLIC,
					},
					{
						Uid:    "be850d18695e433e884c2628a2b4aff2",
						Type:   service.TypeOf_CONTAINER,
						Access: service.Access_PUBLIC,
					},
					{
						Uid:    "c21d91ea8c27493fa02c7a4c709e013d",
						Type:   service.TypeOf_CONTAINER,
						Access: service.Access_PUBLIC,
					},
				}
				Expect(res.GetObjects()).Should(Equal(expected))
			})
		})
	})
})

func getMessageFromResponseBody(r io.Reader, msg proto.Message) proto.Message {
	body, err := ioutil.ReadAll(r)
	Expect(err).Should(BeNil())
	msg, err = stringToMessage(string(body), msg)
	Expect(err).Should(BeNil())

	return msg
}

func getUrl(uri string) string {
	hostName = os.Getenv("TEST_HOST")
	if len(hostName) == 0 {
		panic("Отсутствует путь к хосту в переменной окружения TEST_HOST")
	}

	envPort := os.Getenv("TEST_PORT")
	if len(envPort) > 0 {
		hostName = hostName + ":" + envPort
	}

	return "http://" + hostName + uri
}

