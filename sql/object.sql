CREATE DATABASE IF NOT EXISTS `objects`
  COLLATE 'utf8_general_ci'
  DEFAULT CHARSET 'utf8';

USE objects;

CREATE TABLE `objects` (
  `uid`  BINARY(16) NOT NULL COMMENT 'Идентификатор объектов.',
  `type`   INT,
  `access` INT,
  PRIMARY KEY `obj_uid` (`uid`)
)
  ENGINE = InnoDB
  DEFAULT CHARSET = utf8
  COMMENT 'объекты';

INSERT INTO `objects` (`uid`, `type`, `access`)
    VALUES (UNHEX("85731ebe56924ee592f268df12b847ca"), 1,  0);
INSERT INTO `objects` (`uid`, `type`, `access`)
  VALUES (UNHEX("c21d91ea8c27493fa02c7a4c709e013d"), 1,  0);
INSERT INTO `objects` (`uid`, `type`, `access`)
  VALUES (UNHEX("be850d18695e433e884c2628a2b4aff2"), 1,  0);