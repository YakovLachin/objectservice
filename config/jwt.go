package config

import "fmt"

func GetJWTSecretKey(prefix string ) (string, error) {
	secret, err := GetConfigValue(prefix + "_JWT_SECRET_KEY")
	if err != nil {
		err = fmt.Errorf("Fail Get JWT Secret key from config: %s", err.Error())
	}

	return secret, err
}
