package auth

import (
	"github.com/dgrijalva/jwt-go"
	"errors"
	"fmt"
	"time"
)

type TokenFactory struct {
	Secret string
}

func NewTokenFactory(Secret string) TokenFactory {
	return TokenFactory {
		Secret: Secret,
	}
}

type UIDclaims struct {
	UID string `json:"UID"`
	jwt.StandardClaims
}

func (t *TokenFactory) GetTokenByUID(UID string) (string, error) {
	if len(UID) == 0 {
		return  "", errors.New("Fail GetTokenByUID: empty UID")
	}

	now := time.Now().Unix()
	period := time.Hour * 240



	claims := UIDclaims{
		UID,
		jwt.StandardClaims{
			ExpiresAt: now + int64(period),
		},
	}

	token := jwt.New(jwt.SigningMethodHS256)
	token.Claims = claims

	res, err := token.SignedString([]byte(t.Secret))

	return res, err
}

func (t *TokenFactory) GetUIDByToken(tokenString string) (string, error) {
	if len(tokenString) == 0 {
		return  "", errors.New("Fail GetUIDByToken: empty Token")
	}

	token, err := jwt.ParseWithClaims(tokenString, &UIDclaims{}, func(token *jwt.Token) (interface{}, error) {
		return []byte(t.Secret), nil
	})

	if claims, ok  := token.Claims.(*UIDclaims); ok  && token.Valid {
		return claims.UID, nil
	} else if ve, ok := err.(*jwt.ValidationError); ok {
		if ve.Errors&jwt.ValidationErrorMalformed != 0 {
			return "", errors.New("That's not even a token")
		} else if ve.Errors&(jwt.ValidationErrorExpired|jwt.ValidationErrorNotValidYet) != 0 {
			// Token is either expired or not active yet
			return "", errors.New("Timing is everything")
		} else {
			return "", fmt.Errorf("Couldn't handle this token: %s", err.Error())
		}
	}

	return "", fmt.Errorf("Couldn't handle this token: %s", err.Error())
}
