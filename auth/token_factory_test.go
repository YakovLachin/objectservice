package auth

import (
	"testing"
	"github.com/stretchr/testify/assert"
)

func TestGetTokenByUID(t *testing.T) {
	factory := NewTokenFactory("secret")
	uid := "uid"
	token, err := factory.GetTokenByUID(uid)
	assert.NoError(t, err)
	assert.NotEmpty(t, token)
	assert.NotEqual(t, token, uid)
	parsedUID, err := factory.GetUIDByToken(token)
	assert.NoError(t, err)
	assert.Equal(t, "uid", parsedUID)
}
